package com.ragasi.clamav;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClamAVClientTests {

    private static final String host = "localhost";
    private static final int port = 3310;

    @Test
    public void pingReturnsTrue() {

        // Arrange
        ClamAVClient client = new ClamAVClient(host, port);

        // Act
        boolean response = client.ping();

        // Assert
        assertThat("ping() should return true", response, is(true));
    }

    @Test
    public void versionReturnsString() {

        // Arrange
        ClamAVClient client = new ClamAVClient(host, port);

        // Act
        String version = client.version();

        // Assert
        assertThat("version() should return a string with version info", version, startsWith("ClamAV"));
    }

    @Test
    public void scanWithSafeInputReturnsClean() {

        // Arrange
        ClamAVClient client = new ClamAVClient(host, port);
        String safeString = "a safe string that should scan as clean";
        InputStream inputStream = new ByteArrayInputStream(safeString.getBytes(StandardCharsets.US_ASCII));

        // Act
        ScanResult scanResult = client.scan(inputStream);

        // Assert
        assertThat("scan() should return non-null response", scanResult, notNullValue());
        assertThat("scan() should return result CLEAN", scanResult.result, equalTo(ScanResult.Result.CLEAN));
        assertThat("scan() should return message 'Passed.'", scanResult.message, equalTo("Passed."));
    }

    @Test
    public void scanWithMaliciousInputReturnsThreat() {

        // Arrange
        ClamAVClient client = new ClamAVClient(host, port);
        byte[] maliciousBytes = {
                'X', '5', 'O', '!', 'P', '%', '@', 'A', 'P', '[',
                '4', '\\', 'P', 'Z', 'X', '5', '4', '(', 'P', '^',
                ')', '7', 'C', 'C', ')', '7', '}', '$', 'E', 'I',
                'C', 'A', 'R', '-', 'S', 'T', 'A', 'N', 'D', 'A',
                'R', 'D', '-', 'A', 'N', 'T', 'I', 'V', 'I', 'R',
                'U', 'S', '-', 'T', 'E', 'S', 'T', '-', 'F', 'I',
                'L', 'E', '!', '$', 'H', '+', 'H', '*' };
        InputStream inputStream = new ByteArrayInputStream(maliciousBytes);

        // Act
        ScanResult scanResult = client.scan(inputStream);

        // Assert
        assertThat("scan() should return non-null response", scanResult, notNullValue());
        assertThat("scan() should return result THREAT", scanResult.result, equalTo(ScanResult.Result.THREAT));
        assertThat("scan() should return threat name", scanResult.message, equalTo("Eicar-Test-Signature"));
    }

    @Test
    public void reloadReturnsTrue() {

        // Arrange
        ClamAVClient client = new ClamAVClient(host, port);

        // Act
        boolean response = client.reload();

        // Assert
        assertThat("reload() should return true", response, is(true));
    }

    @Test
    public void statsReturnsString() {

        // Arrange
        ClamAVClient client = new ClamAVClient(host, port);

        // Act
        String stats = client.stats();

        // Assert
        assertThat("stats() should return a string with stats info", stats, endsWith("END\0"));
    }
}
