package com.ragasi.clamav;

public class ClamAVClientException extends RuntimeException {

    ClamAVClientException() {
        super();
    }

    ClamAVClientException(String message) {
        super(message);
    }
}
