package com.ragasi.clamav;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClamAVClient {

    private static final int DEFAULT_TIMEOUT_MILLIS = 1000;

    private final String host;
    private final int port;
    private final int timeoutMillis;

    ClamAVClient(String host, int port) {
        this(host, port, DEFAULT_TIMEOUT_MILLIS);
    }

    ClamAVClient(String host, int port, int timeoutMillis) {
        if (timeoutMillis < 0) {
            throw new IllegalArgumentException("Invalid timeout specified.");
        }

        this.host = host;
        this.port = port;
        this.timeoutMillis = timeoutMillis;
    }

    boolean ping() throws ClamAVClientException {

        return execute("zPING\0", "PONG");
    }

    String version() throws ClamAVClientException {

        try (final Socket socket = new Socket(host, port);
             final OutputStream outputStream = socket.getOutputStream()) {

            socket.setSoTimeout(timeoutMillis);
            outputStream.write("zVERSION\0".getBytes(StandardCharsets.US_ASCII));
            outputStream.flush();

            final byte[] buffer = new byte[2048];
            final int readCount = socket.getInputStream().read(buffer, 0, buffer.length);

            if (readCount == -1) {
                throw new ClamAVClientException("Invalid response.");
            }

            return new String(buffer, 0, readCount, StandardCharsets.US_ASCII);
        }
        catch (IOException e) {
            throw new ClamAVClientException("Connection error occurred.");
        }
    }

    ScanResult scan(InputStream inputStream) throws ClamAVClientException {

        try (final Socket socket = new Socket(host, port);
             final OutputStream outputStream = socket.getOutputStream()) {

            socket.setSoTimeout(timeoutMillis);
            outputStream.write("zINSTREAM\0".getBytes(StandardCharsets.US_ASCII));
            outputStream.flush();

            final byte[] chunk = new byte[2048];
            boolean respondedEarly = false;

            try (final InputStream responseStream = socket.getInputStream()) {
                int readCount = inputStream.read(chunk);
                while (readCount > 0) {
                    final byte[] chunkSize = ByteBuffer.allocate(4).putInt(readCount).array();

                    outputStream.write(chunkSize);
                    outputStream.write(chunk, 0, readCount);

                    if (responseStream.available() > 0) {
                        respondedEarly = true;
                        break;
                    }

                    readCount = inputStream.read(chunk);
                }

                if (!respondedEarly) {
                    outputStream.write(new byte[]{0, 0, 0, 0});
                    outputStream.flush();
                }

                final byte[] response = toByteArray(responseStream);

                String message = new String(response, StandardCharsets.US_ASCII);

                if (message.equals("stream: OK\0")) {
                    return ScanResult.clean("Passed.");
                }
                else {
                    Matcher matcher;

                    matcher = Pattern.compile("stream: (.*) FOUND\0").matcher(message);
                    if (matcher.matches()) {
                        return ScanResult.threat(matcher.group(1));
                    }

                    matcher = Pattern.compile("INSTREAM (.*) ERROR\0").matcher(message);
                    if (matcher.matches()) {
                        return ScanResult.error(matcher.group(1));
                    }

                    return ScanResult.error("Unrecognized response.");
                }
            }
        }
        catch (IOException e) {
            throw new ClamAVClientException("Connection error occurred.");
        }
    }

    boolean reload() throws ClamAVClientException {

        return execute("zRELOAD\0", "RELOADING");
    }

    String stats() throws ClamAVClientException {

        try (final Socket socket = new Socket(host, port);
             final OutputStream outputStream = socket.getOutputStream()) {

            socket.setSoTimeout(timeoutMillis);
            outputStream.write("zSTATS\0".getBytes(StandardCharsets.US_ASCII));
            outputStream.flush();

            final byte[] response = toByteArray(socket.getInputStream());

            return new String(response, StandardCharsets.US_ASCII);
        }
        catch (IOException e) {
            throw new ClamAVClientException("Connection error occurred.");
        }
    }

    private boolean execute(String command, String expects) throws ClamAVClientException {
        return execute(command.getBytes(StandardCharsets.US_ASCII), expects.getBytes(StandardCharsets.US_ASCII));
    }

    private boolean execute(byte[] command, byte[] expects) throws ClamAVClientException {

        try (final Socket socket = new Socket(host, port);
             final OutputStream outputStream = socket.getOutputStream()) {

            socket.setSoTimeout(timeoutMillis);
            outputStream.write(command);
            outputStream.flush();

            final byte[] buffer = new byte[expects.length];
            final int readCount = socket.getInputStream().read(buffer, 0, buffer.length);

            return readCount != -1 && Arrays.equals(buffer, expects);
        }
        catch (IOException e) {
            throw new ClamAVClientException("Connection error occurred.");
        }
    }

    private byte[] toByteArray(InputStream stream) throws IOException {

        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        final byte[] buffer = new byte[4096];

        int readCount = stream.read(buffer);
        while (readCount != -1) {
            output.write(buffer, 0, readCount);
            readCount = stream.read(buffer);
        }

        return output.toByteArray();
    }
}
