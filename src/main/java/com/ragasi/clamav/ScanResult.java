package com.ragasi.clamav;

public class ScanResult {

    enum Result {
        CLEAN,
        ERROR,
        THREAT
    }

    public final Result result;
    public final String message;

    public static ScanResult clean(String message) {
        return new ScanResult(Result.CLEAN, message);
    }

    public static ScanResult threat(String threat) {
        return new ScanResult(Result.THREAT, threat);
    }

    public static ScanResult error(String message) {
        return new ScanResult(Result.ERROR, message);
    }

    private ScanResult(Result result, String message) {
        this.result = result;
        this.message = message;
    }
}
