package com.ragasi.clamav;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("clam")
public class ScannerApi {

    private ClamAVClient client = new ClamAVClient("localhost", 3310);

    @RequestMapping("ping")
    public String getHealth() {
        if (client.ping()) {
            return "Healthy.";
        }

        return "Not healthy. =(";
    }

    @RequestMapping("version")
    public String version() {
        return client.version();
    }

    @PostMapping("scan")
    public @ResponseBody ScanResult scan(@RequestParam("file") MultipartFile file) {

        if (file.isEmpty()) {
            return ScanResult.error("No file or file is empty.");
        }

        try {
            return client.scan(file.getInputStream());
        }
        catch (ClamAVClientException e) {
            return ScanResult.error(e.getMessage());
        }
        catch (IOException e) {
            return ScanResult.error("Unable to read file.");
        }
    }

    @PostMapping("reload")
    public String reload() {
        if (client.reload()) {
            return "Reload initiated.";
        }

        return "Something went wrong.";
    }

    @RequestMapping("stats")
    public String stats() {
        return client.stats();
    }
}
